package com.kingpowerclicktest.framework.mvvm.data.repository

import com.kingpowerclicktest.framework.mvvm.data.api.ApiHelper
import com.kingpowerclicktest.framework.mvvm.data.model.Album
import io.reactivex.Single

class MainRepository(private val apiHelper: ApiHelper) {

    fun getAlbum(): Single<List<Album>> {
        return apiHelper.getAlbum()
    }

}