package com.kingpowerclicktest.framework.mvvm.ui.main.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.kingpowerclicktest.framework.mvvm.R
import com.kingpowerclicktest.framework.mvvm.data.model.Album
import kotlinx.android.synthetic.main.item_layout.view.*


class MainAdapter(
    private val album: ArrayList<Album>
) : RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(user: Album) {
            itemView.textViewUserName.text = user.name

            val url = GlideUrl(
                user.avatar, LazyHeaders.Builder()
                    .addHeader("User-Agent", "your-user-agent")
                    .build()
            )
            Glide.with(itemView.imageViewAvatar.context)
                .load(url)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .override(150,150)
                .into(itemView.imageViewAvatar)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_layout, parent,
                false
            )
        )

    override fun getItemCount(): Int = album.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(album[position])

    override fun getItemId(position: Int): Long {
        Log.d("CheckItem"," : "+position)
        return position.toLong()
    }


    fun addData(list: List<Album>) {
        album.addAll(list)
    }

}