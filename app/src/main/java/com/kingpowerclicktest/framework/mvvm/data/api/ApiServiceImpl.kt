package com.kingpowerclicktest.framework.mvvm.data.api

import com.kingpowerclicktest.framework.mvvm.data.model.Album
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Single

class ApiServiceImpl : ApiService {

    override fun getAlbum(): Single<List<Album>> {
        return Rx2AndroidNetworking.get("https://jsonplaceholder.typicode.com/albums/1/photos")
            .build()
            .getObjectListSingle(Album::class.java)
    }

}