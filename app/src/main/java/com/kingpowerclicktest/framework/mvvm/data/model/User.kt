package com.kingpowerclicktest.framework.mvvm.data.model

import com.google.gson.annotations.SerializedName

data class Album(
    @SerializedName("albumId")
    val albumId: Int = 0,
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("title")
    val name: String = "",
    @SerializedName("url")
    val email: String = "",
    @SerializedName("thumbnailUrl")
    val avatar: String = ""
)