package com.kingpowerclicktest.framework.mvvm.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}