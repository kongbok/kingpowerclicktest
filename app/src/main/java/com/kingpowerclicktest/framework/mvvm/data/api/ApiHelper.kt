package com.kingpowerclicktest.framework.mvvm.data.api

class ApiHelper(private val apiService: ApiService) {

    fun getAlbum() = apiService.getAlbum()

}