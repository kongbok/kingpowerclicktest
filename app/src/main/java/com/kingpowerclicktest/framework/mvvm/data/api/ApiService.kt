package com.kingpowerclicktest.framework.mvvm.data.api

import com.kingpowerclicktest.framework.mvvm.data.model.Album
import io.reactivex.Single

interface ApiService {

    fun getAlbum(): Single<List<Album>>

}